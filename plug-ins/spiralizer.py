import sys
import math
import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

# node name
kPluginNodeTypeName = 'spiralizerCurve'
spiralizerID = om.MTypeId(0x00000005)

# Attribute names
curveInputLong = 'inputCurve'
curveInputShort = 'ic'
clockwiseLong = 'clockwise'
clockwiseShort = 'cw'
percentLong = 'percent'
percentShort = 'p'
samplesLong = 'samples'
samplesShort = 'sp'
turnsLong = 'turns'
turnsShort = 't'
radiusLong = 'radius'
radiusShort = 'r'
radiusRampLong = 'radiusRamp'
radiusRampShort = 'rr'
radiusOptionsLong = 'radiusOptions'
radiusOptionsShort = 'rop'
distributionRampLong = 'distribution'
distributionRampShort = 'dt'
distributionOptionsLong = 'distributionOptions'
distributionOptionsShort = 'do'
offsetLong = 'offset'
offsetShort = 'off'
outputCurveLong = 'outputCurve'
outputCurveShort = 'oc'

# Create class for spiralizer node
class SpiralizerNode(ompx.MPxNode):
    # Class variables (inputs and outputs plugs)
    curveInput = om.MObject()
    clockwiseInput = om.MObject()
    percentInput = om.MObject()
    samplesInput = om.MObject()
    turnsInput = om.MObject()
    radiusInput = om.MObject()
    radiusRampInput = om.MObject()
    radiusRampOptionsInput = om.MObject()
    distributionRampInput = om.MObject()
    distributionOptionsInput = om.MObject()
    offsetInput = om.MObject()
    curveOutput = om.MObject()

    # override
    def __init__(self):
        ompx.MPxNode.__init__(self)
        # Both MFn classes are slow in initialization. So, it is preferable to reuse them throughout the process.
        self.mfnCurve = om.MFnNurbsCurve()
        self.mfnDepNode = om.MFnDependencyNode()


    # override
    def postConstructor(self, *args):
        # Initialize radiusRampInput
        # Get MPlug of radiusRampInput and use to initialize a MRampAttribute to set new values.
        plug = om.MPlug(self.thisMObject(), SpiralizerNode.radiusRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # radiusRampInput will be a constant graph
            # |o-------|
            # |________|
            rampAttr.setValueAtIndex(1.0, 0)
        except:
            pass

        # Initialize distributionRampInput
        # Get MPlug of distributionRampInput and use to initialize a MRampAttribute to set a new values
        plug = om.MPlug(self.thisMObject(), SpiralizerNode.distributionRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # distributionRampInput will be an ascending graph
            # |    /o|
            # |  /   |
            # |o/____|
            # Set first index to 0.0 value
            rampAttr.setValueAtIndex(0.0, 0)

            # Add new index (1) ...
            indexArray = om.MFloatArray()
            indexArray.append(1.0)
            # with value 1.0 ...
            valueArray = om.MFloatArray()
            valueArray.append(1.0)
            # and set interpolation to linear.
            interpArray = om.MIntArray()
            interpArray.append(1)

            rampAttr.addEntries(indexArray,
                                valueArray,
                                interpArray)
        except :
            pass


    # override
    def compute(self, plug, dataBlock):
        # Verify if have something connected in curveOutput plug.
        # Otherwise, SpiralizerNode will not process nothing
        if plug == SpiralizerNode.curveOutput:
            ############################################################################################################
            # Before start process, need get some handles to access external informations.
            #
            # Create input curve handle
            curveHandle = dataBlock.inputValue(SpiralizerNode.curveInput)
            curveObjInput = curveHandle.asNurbsCurveTransformed()

            # Create clockwise boolean handle
            clockwiseHandle = dataBlock.inputValue(SpiralizerNode.clockwiseInput)
            clockwiseStatus = clockwiseHandle.asBool()

            # create percent value handle
            percentHandle = dataBlock.inputValue(SpiralizerNode.percentInput)
            percentValue = percentHandle.asFloat()
            percentValue *= 0.01 # normalize to 0.0 - 1.0

            # Create samples handle
            samplesHandle = dataBlock.inputValue(SpiralizerNode.samplesInput)
            samplesValue = samplesHandle.asInt()

            # Create turn handle
            turnHandle = dataBlock.inputValue(SpiralizerNode.turnsInput)
            turnValue = turnHandle.asFloat()

            # Create radius handle
            radiusHandle = dataBlock.inputValue(SpiralizerNode.radiusInput)
            radiusValue = radiusHandle.asFloat()

            # Create rampRadius MRampAttribute (in case of MRamps we need use MRampAttribute to access this plug)
            rampRadiusAttr = om.MRampAttribute(self.thisMObject(), SpiralizerNode.radiusRampInput)

            # Create radiusRampOptions handle
            rampRadiusOptionsHandle = dataBlock.inputValue(SpiralizerNode.radiusRampOptionsInput)
            rampRadiusOptionsValue = rampRadiusOptionsHandle.asShort()

            # Create rampDistribution MRampAttribute
            rampDistribAttr = om.MRampAttribute(self.thisMObject(), SpiralizerNode.distributionRampInput)

            # Create distributionRampOptions handle
            rampDistributionOptionsHandle = dataBlock.inputValue(SpiralizerNode.distributionOptionsInput)
            rampDistributionOptionsValue = rampDistributionOptionsHandle.asShort()

            # Create offset handle
            offsetHandle = dataBlock.inputValue(SpiralizerNode.offsetInput)
            offsetValue = offsetHandle.asFloat()

            # Create output curve handle
            outputCurveHandle = dataBlock.outputValue(SpiralizerNode.curveOutput)

            ############################################################################################################
            # Process to define up-vector and front-vector of curve
            #

            self.mfnCurve.setObject(curveObjInput)
            # Initialize vectors of transform curve
            curveTransformXVector = om.MVector().xAxis
            curveTransformYVector = om.MVector().yAxis
            curveTransformZVector = om.MVector().zAxis

            initialUpVector = om.MVector()
            initialFrontVector = om.MVector()
            initialSideVector = om.MVector()

            # Get inclusiveMatrix from input curve
            thisNode = SpiralizerNode.thisMObject(self)
            self.mfnDepNode.setObject(thisNode)
            # Get MPlug of curveInput. Its necessary to access curve node..
            # to get your dagPath to this curve.
            inputCurveMPlug = self.mfnDepNode.findPlug(SpiralizerNode.curveInput)
            curveShapeMPlug = inputCurveMPlug.source()
            curveShapeMObject = curveShapeMPlug.node()
            curveShapeDagPath = om.MDagPath()
            om.MDagPath.getAPathTo(curveShapeMObject, curveShapeDagPath)
            # With a dagPath to curve, we can get inclusiveMatrix
            inclusiveMatrixCurve = curveShapeDagPath.inclusiveMatrix()

            # Use inclusiveMatrixCurve to calculate a new values to initial vectors
            curveTransformXVector = curveTransformXVector * inclusiveMatrixCurve
            curveTransformYVector = curveTransformYVector * inclusiveMatrixCurve
            curveTransformZVector = curveTransformZVector * inclusiveMatrixCurve

            # Use MItCurveCV to get firsts CV points to calculate the initial vector of curve
            mItCurve = om.MItCurveCV(curveObjInput)
            firstCV = om.MPoint()
            secondCV = om.MPoint()

            while not mItCurve.isDone():
                # Get first CV position
                if mItCurve.index() == 0:
                    firstCV = mItCurve.position()
                else:
                    secondCV = mItCurve.position()
                    # If firstCV and secondCV have the same position, need get other CV
                    if not firstCV == secondCV:
                        break
                mItCurve.next()

            initialFrontVector = om.MVector(secondCV - firstCV)
            initialFrontVector.normalize()

            # Calculate 3 initial vectors based in initialFrontVector
            # 3 firsts cases work if the initialFrontVector is the same as one of the orthogonal vectors
            if initialFrontVector.isParallel(curveTransformYVector):
                initialSideVector = curveTransformZVector
                initialUpVector = (curveTransformXVector * -1 if initialFrontVector.isEquivalent(curveTransformYVector)
                                   else curveTransformYVector)
            elif initialFrontVector.isParallel(curveTransformXVector):
                initialSideVector = (curveTransformZVector if initialFrontVector.isEquivalent(curveTransformXVector)
                                     else curveTransformZVector * -1)
            elif initialFrontVector.isParallel(curveTransformZVector):
                initialSideVector = (curveTransformXVector * -1 if initialFrontVector.isEquivalent(curveTransformZVector)
                                     else curveTransformXVector)
                initialUpVector = curveTransformYVector
            else: # common case
                _tmpYAxis = curveTransformYVector
                initialSideVector = initialFrontVector ^ _tmpYAxis
                initialSideVector.normalize()
                initialUpVector = initialSideVector ^ initialFrontVector
                initialUpVector.normalize()

            ############################################################################################################
            # Start process to calculate each CV (samples) of spiral along path
            #

            # Calculate the percentage of the curve that will be used.
            # Get total length of path
            totalLengthCurveInput = self.mfnCurve.length()
            # Calculate percent length
            lengthCurve = totalLengthCurveInput * percentValue

            # Calculate the total amount of samples along the path
            _calculatedSamples = int(samplesValue) * int(turnValue)
            _calculatedSamples = (1 if _calculatedSamples == 0 else _calculatedSamples)
            lengthPart = lengthCurve / _calculatedSamples

            # Calculate the turn value for each sample
            completeTurn = (2 * math.pi) * turnValue
            turnPart = (completeTurn / _calculatedSamples)

            # Create a NurbsCurveData to store all data of new curve
            curveData = om.MFnNurbsCurveData()
            curveObj = curveData.create()

            # Create empty MPointArray to store vertex/CV coordinates
            controlVertices = om.MPointArray()

            # Variables to represent local vectors for each sample of input curve
            localFrontVector = om.MVector()
            localSideVector = om.MVector()
            localUpVector = om.MVector()

            # This condition guarantee processing only if percent great than 0
            if percentValue > 0:
                for i in range(_calculatedSamples):
                    # Calculate the current raw length of the path based on percent value.
                    currentLength = i * lengthPart
                    # User can choose between two mapping types of ramps
                    if rampDistributionOptionsValue == 0:
                        # Calculate percent of totalLengthCurveInput (path)
                        percentLength = currentLength / totalLengthCurveInput
                    else:
                        # Calculate percent of lengthCurve
                        percentLength = currentLength / lengthCurve
                    # OMG.. MScriptUtil to get reference values. Get length value based on ramps.
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    rampDistribAttr.getValueAtPosition(percentLength, util_ptr)
                    percentResult = om.MScriptUtil.getFloat(util_ptr)

                    # currentLength result in new value based on ramps
                    currentLength = lengthCurve * percentResult
                    # Get curve param to current length of path
                    curveParameter = self.mfnCurve.findParamFromLength(currentLength)

                    # Get coords from current sample (current length)
                    pointPosition = om.MPoint()
                    self.mfnCurve.getPointAtParam(curveParameter, pointPosition)

                    # Get tangent from current samples (current length)
                    tangent = om.MVector()
                    tangent = self.mfnCurve.tangent(curveParameter)

                    # Calculate local vectors of current pointPosition
                    # At first sample, use initial vectors as a base
                    if i == 0:
                        localFrontVector = tangent
                        localSideVector = localFrontVector ^ initialUpVector
                        localUpVector = localSideVector ^ localFrontVector

                    # Others samples
                    else:
                        localFrontVector = tangent
                        localSideVector = localFrontVector ^ localUpVector
                        localUpVector = localSideVector ^ localFrontVector

                    localFrontVector.normalize()
                    localSideVector.normalize()
                    localUpVector.normalize()

                    # Create initial coordinates to new spiralControlVertice
                    _currentCurveValue = (turnPart * i)
                    # Get current relative radius value using MRamp
                    if rampRadiusOptionsValue == 0:
                        percentCurve = (i * lengthPart)/totalLengthCurveInput
                    elif rampRadiusOptionsValue == 1:
                        percentCurve = (i * lengthPart) / lengthCurve
                    else:
                        percentCurve = (i * lengthPart) / lengthCurve
                    # Again.. more MScriptUtils...
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    rampRadiusAttr.getValueAtPosition(percentCurve, util_ptr)
                    currentRadiusValue = om.MScriptUtil.getFloat(util_ptr)

                    # Control clockwise or counterclockwise
                    _resultRadius = currentRadiusValue * radiusValue
                    if clockwiseStatus:

                        x = 0
                        y = math.cos(_currentCurveValue - offsetValue) * _resultRadius
                        z = math.sin(_currentCurveValue - offsetValue) * _resultRadius
                        spiralControlVertice = om.MVector(x, y, z)
                    else:

                        x = 0
                        y = math.sin(_currentCurveValue - offsetValue) * _resultRadius
                        z = math.cos(_currentCurveValue - offsetValue) * _resultRadius
                        spiralControlVertice = om.MVector(x, y, z)

                    # Calculate correct 'aim/front vector' orientation to new point
                    newPointFrontVector = om.MVector().xAxis
                    newPointFrontQuaternion = om.MQuaternion(newPointFrontVector, localFrontVector)

                    # Calculate 'up' vector
                    tmpUpVector = om.MVector().yAxis
                    tmpUpVector = tmpUpVector.rotateBy(newPointFrontQuaternion)
                    angle = tmpUpVector.angle(localUpVector)
                    tmpQuaternion = om.MQuaternion(angle, localFrontVector)
                    if not localUpVector.isEquivalent(tmpUpVector.rotateBy(tmpQuaternion), 1.0e-4):
                        angle = (2 * math.pi) - angle
                        tmpQuaternion = om.MQuaternion(angle, localFrontVector)

                    # Here we union two transform (aim/front vector and up vector to one single quaternion) to apply
                    # the transformation on initial coordinates of spiral (spiralControlVertice)
                    newPointFrontQuaternion *= tmpQuaternion
                    spiralControlVertice = spiralControlVertice.rotateBy(newPointFrontQuaternion)

                    # Finally, adjust spiralControlVertice to new position using MTransformMatrix.
                    tMat = om.MTransformationMatrix()
                    tMat.setTranslation(om.MVector(pointPosition), om.MSpace.kWorld)
                    tmpMatrix = tMat.asMatrix()
                    # We need convert spiralControlVertice(MVector) to MPoint for use with MTransformMatrix
                    spiralControlVertice = om.MPoint(spiralControlVertice)
                    spiralControlVertice = spiralControlVertice * tmpMatrix

                    # Now, append this new coordinate to the array. The spiral will be created after loop.
                    controlVertices.append(spiralControlVertice)

                # Using controlVertices(MPointArray) to create a spiral em memory
                self.mfnCurve.createWithEditPoints(controlVertices,
                                                   3,
                                                   om.MFnNurbsCurve.kOpen,
                                                   False,
                                                   False,
                                                   False,
                                                   curveObj)
            outputCurveHandle.setMObject(curveObj)
            dataBlock.setClean(plug)
        else:
            return om.kUnknownParameter


def spiralizerNodeCreator():
    return ompx.asMPxPtr(SpiralizerNode())


def spiralizerInitializer():
    # Initialize all node attributes with your types.
    nAttr = om.MFnNumericAttribute()
    typedAttr = om.MFnTypedAttribute()
    eAttr = om.MFnEnumAttribute()

    ####################################################################################################################
    # curveInput
    SpiralizerNode.curveInput = typedAttr.create(curveInputLong,
                                                 curveInputShort,
                                                 om.MFnNurbsCurveData.kNurbsCurve)
    typedAttr.setReadable(False)
    typedAttr.setWritable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.curveInput)

    ####################################################################################################################
    # clockwiseInput
    SpiralizerNode.clockwiseInput = nAttr.create(clockwiseLong,
                                                 clockwiseShort,
                                                 om.MFnNumericData.kBoolean, True)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.clockwiseInput)

    ####################################################################################################################
    # percentInput
    SpiralizerNode.percentInput = nAttr.create(percentLong,
                                               percentShort,
                                               om.MFnNumericData.kFloat)
    nAttr.setMin(0.0)
    nAttr.setMax(100.0)
    nAttr.setDefault(100.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.percentInput)

    ####################################################################################################################
    # sampleInput
    SpiralizerNode.samplesInput = nAttr.create(samplesLong,
                                               samplesShort,
                                               om.MFnNumericData.kInt, 4)
    nAttr.setMin(3)
    SpiralizerNode.addAttribute(SpiralizerNode.samplesInput)

    ####################################################################################################################
    # turnsInput
    SpiralizerNode.turnsInput = nAttr.create(turnsLong,
                                             turnsShort,
                                             om.MFnNumericData.kFloat, 10.0)
    nAttr.setMin(1)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.turnsInput)

    ####################################################################################################################
    # radiusInput
    SpiralizerNode.radiusInput = nAttr.create(radiusLong,
                                              radiusShort,
                                              om.MFnNumericData.kFloat, 1.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.radiusInput)

    ####################################################################################################################
    # radiusRampInput
    SpiralizerNode.radiusRampInput = om.MRampAttribute().createCurveRamp(radiusRampLong,
                                                                         radiusRampShort)
    SpiralizerNode.addAttribute(SpiralizerNode.radiusRampInput)

    ####################################################################################################################
    # radiusRampOptionsInput
    SpiralizerNode.radiusRampOptionsInput = eAttr.create(radiusOptionsLong,
                                                         radiusOptionsShort)
    eAttr.addField('Full path', 0)
    eAttr.addField('Spiral', 1)
    eAttr.setWritable(True)
    eAttr.setStorable(True)
    eAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.radiusRampOptionsInput)

    ####################################################################################################################
    # distributionRampInput
    SpiralizerNode.distributionRampInput = om.MRampAttribute().createCurveRamp(distributionRampLong,
                                                                               distributionRampShort)
    SpiralizerNode.addAttribute(SpiralizerNode.distributionRampInput)

    ####################################################################################################################
    # distributionOptionsInput
    SpiralizerNode.distributionOptionsInput = eAttr.create(distributionOptionsLong,
                                                           distributionOptionsLong)
    eAttr.addField('Full path', 0)
    eAttr.addField('Spiral', 1)
    eAttr.setWritable(True)
    eAttr.setStorable(True)
    eAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.distributionOptionsInput)

    ####################################################################################################################
    # offsetInput
    SpiralizerNode.offsetInput = nAttr.create(offsetLong,
                                              offsetShort,
                                              om.MFnNumericData.kFloat, 0.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerNode.addAttribute(SpiralizerNode.offsetInput)

    ####################################################################################################################
    # curveOutput
    SpiralizerNode.curveOutput = typedAttr.create(outputCurveLong,
                                                  outputCurveShort,
                                                  om.MFnNurbsCurveData.kNurbsCurve)
    typedAttr.setReadable(True)
    typedAttr.setWritable(False)
    SpiralizerNode.addAttribute(SpiralizerNode.curveOutput)

    ####################################################################################################################
    # relationships

    SpiralizerNode.attributeAffects(SpiralizerNode.curveInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.clockwiseInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.percentInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.samplesInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.turnsInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.radiusInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.radiusRampInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.radiusRampOptionsInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.distributionRampInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.distributionOptionsInput, SpiralizerNode.curveOutput)
    SpiralizerNode.attributeAffects(SpiralizerNode.offsetInput, SpiralizerNode.curveOutput)


########################################################################################################################
########################################################################################################################
# Create command to apply Spiralizer in a curve/path
kCmdName = 'makeSpiral'

class SpiralizerCmd(ompx.MPxCommand):
    def __init__(self):
        ompx.MPxCommand.__init__(self)
        self.mArgList = om.MArgList()
        self.mdgMod = om.MDGModifier()
        self.mdagMod = om.MDagModifier()
        self.sel = om.MSelectionList()
        self.mfnDep = om.MFnDependencyNode()


    # override
    def isUndoable(self):
        return True


    # override
    def doIt(self, mArgList):
        # Clear all variables
        self.sel.clear()

        # Get size of mArgList
        sizeMArglist = mArgList.length()
        # If have some argument...
        if sizeMArglist > 0:
            for i in range(sizeMArglist):
                arg = mArgList.asString(i)
                try:
                    self.sel.add(arg)
                except RuntimeError:
                    print '"{}" not exist'.format(arg)
        # otherwise get selected curves
        else:
            om.MGlobal.getActiveSelectionList(self.sel)

        self.redoIt()


    # override
    def undoIt(self):
        self.mdagMod.undoIt()
        self.mdgMod.undoIt()


    # override
    def redoIt(self):
        # Reset mdgMod and mdagMod
        self.mdgMod = om.MDGModifier()
        self.mdagMod = om.MDagModifier()

        # Iterate over all sel list
        mItSelectionList = om.MItSelectionList(self.sel)
        # If confirmed the selection is a curve, connect it with a Spiralizer
        while not mItSelectionList.isDone():
            selectionObj = om.MObject()
            mItSelectionList.getDependNode(selectionObj)
            # Verify if selection is a kTransform
            if selectionObj.apiType() == om.MFn.kTransform:
                # Need get dagPath to get your child (shape)
                selectionDagPath = om.MDagPath()
                om.MDagPath.getAPathTo(selectionObj, selectionDagPath)
                # Get curve name
                originalCurveNode = selectionDagPath.node()
                self.mfnDep.setObject(originalCurveNode)
                originalCurveName = self.mfnDep.name()
                originalCurveName = originalCurveName.replace(':', '_')
                if selectionDagPath.childCount() > 0:
                    # Verify if child is a nurbsCurve
                    child = selectionDagPath.child(0)
                    if child.apiType() == om.MFn.kNurbsCurve:
                        self.mfnDep.setObject(child)
                        curveMPlugArray = self.mfnDep.findPlug('worldSpace', True)
                        curveWorldSpaceMPlug = curveMPlugArray.elementByLogicalIndex(0)

                        # Create a new Spiralizer node and made a connection with this spline
                        spiralizerNode = self.mdgMod.createNode(kPluginNodeTypeName)
                        self.mfnDep.setObject(spiralizerNode)
                        spiralMPlug = self.mfnDep.findPlug(curveInputLong)
                        self.mdgMod.connect(curveWorldSpaceMPlug, spiralMPlug)
                        self.mdgMod.doIt()

                        # Create a new nurbsCurve (transform)
                        resultCurveTransform = self.mdagMod.createNode('nurbsCurve')
                        self.mdagMod.renameNode(resultCurveTransform, 'spiral_{}'.format(originalCurveName))
                        self.mdagMod.doIt()

                        # Get child from resultCurveTransform
                        resultCurveDagPath = om.MDagPath()
                        om.MDagPath.getAPathTo(resultCurveTransform, resultCurveDagPath)
                        resultCurveShape = resultCurveDagPath.child(0)

                        # Get MPlugs from spiralizer node and resultCurvesShape
                        self.mfnDep.setObject(spiralizerNode)
                        spiralizerOutputCurveMPlug = self.mfnDep.findPlug(outputCurveLong)
                        self.mfnDep.setObject(resultCurveShape)
                        resultCurveCreateMPlug = self.mfnDep.findPlug('create')

                        self.mdgMod.connect(spiralizerOutputCurveMPlug, resultCurveCreateMPlug)
                        self.mdgMod.doIt()


            mItSelectionList.next()


def spiralizerCmdCreator():
    return ompx.asMPxPtr(SpiralizerCmd())

# Create array to store the menu for spiralizer
menuArray = []
# Name subMenu
subMenuName = 'jum'

def initializePlugin(mobj):
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.registerNode(kPluginNodeTypeName,
                             spiralizerID,
                             spiralizerNodeCreator,
                             spiralizerInitializer)
    except:
        sys.stderr.write('Failed to register node: {}'.format(kPluginNodeTypeName))
        raise

    try:
        mplugin.registerCommand(kCmdName,
                                spiralizerCmdCreator)
    except:
        sys.stderr.write('Failed to register cmd: {}'.format(kCmdName))
        raise

    # Add spiralizer to the Create menu
    # Create, if necessary, a jMenu
    if not cmds.menuItem(subMenuName, ex = True):
        cmds.menuItem(subMenuName, subMenu = True, label = subMenuName, parent = 'mainCreateMenu')

    menuCreated = mplugin.addMenuItem('Spiralizer Curve', 'mainCreateMenu|' + subMenuName, 'makeSpiral', '')
    menuArray.append(menuCreated)


def uninitializePlugin(mobj):
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.deregisterNode(spiralizerID)
    except:
        sys.stderr.write('Failed to deregister node: {}'.format(kPluginNodeTypeName))
        raise

    try:
        mplugin.deregisterCommand(kCmdName)
    except:
        sys.stderr.write('Failed to deregister cmd: {}'.format(kCmdName))
        raise

    # Remove spiralizer from Create menu
    mplugin.removeMenuItem(menuArray[0])

    if cmds.menuItem(subMenuName, ex = True):
        childsMenu = cmds.menu(subMenuName, q = True, ia = True)
        if childsMenu == None:
            cmds.deleteUI(subMenuName, menuItem = True)






